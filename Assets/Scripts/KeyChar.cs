﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class KeyChar : MonoBehaviour
{
    [SerializeField] private TMP_Text tmpChar;
    public string Character { get; private set; }
    
    void Start()
    {
        InstChar();
    }

    private void InstChar()
    {
        var rdmKey  =  Random.Range(1, 26);
        Character = Enum.GetName(typeof(Letter), rdmKey);
        tmpChar.text = Character;
    }
}