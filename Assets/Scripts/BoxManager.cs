﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{
    [SerializeField] private KeyPanel panelPrefab;
    [SerializeField] private Transform boxTransform;
    [SerializeField] private BoxEnemy boxPrefab;
    
    private int _countSymbolRound;
    private KeyPanel _currentPanel;
    private BoxEnemy _currentEnemy;
    private string _letterCode;

    private void Start()
    {
        GenerateBox(); 
        StartCoroutine(RespawningBox());
    }

    private void Update()
    {
        
        _letterCode = KeyboardController.LastSymbol;
        if (_letterCode == string.Empty) return;
        if (_letterCode == null) return;
        _currentPanel.CheckAndDestroyKey(_letterCode);
        _letterCode = string.Empty;
    }

    private void GenerateBox()
    {
        _currentEnemy = Instantiate(boxPrefab, null);
        _currentPanel = Instantiate(panelPrefab, boxTransform);
        _currentPanel.transform.localScale = new Vector3(.01f, .01f, .01f);
        _currentEnemy.PanelOnBox = _currentPanel;
        _currentPanel.IsEmptyChars += () => { Destroy(_currentEnemy.gameObject); };
    }

    private IEnumerator RespawningBox()
    {
        yield return new WaitForSeconds(10f);
        Destroy(_currentEnemy.gameObject);
        Destroy(_currentPanel.gameObject);
        GenerateBox();
        StartCoroutine(RespawningBox());
    }
}
