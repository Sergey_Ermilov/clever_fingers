﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class KeyboardController : MonoBehaviour
{
    public static string LastSymbol { get; set; }
    
    private static TouchScreenKeyboard keyboard;
    public string text;
    public TMP_Text tmptext;

    void Start()
    {
        keyboard = TouchScreenKeyboard.Open(text, TouchScreenKeyboardType.Default);
    }

    void Update()
    {
        if (keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done)
        {
            tmptext.text = keyboard.text;
            text = keyboard.text;
            LastSymbol = GetLastSymbol();
            print("User input is: " + text);
        }
    }

    private string GetLastSymbol()
    {
        return  keyboard.text[keyboard.text.Length - 1].ToString();
    }
}
