﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxEnemy : MonoBehaviour
{
    public KeyPanel PanelOnBox { get; set; }
    public float currentSpeedBox => _setSpeedBox;
    
    [SerializeField] private float _setSpeedBox = 1;

    private bool isImpulse;
    private void Update()
    {
        if (transform.position.z <= -6.34999943)
        {
            if(!isImpulse)
                StartCoroutine(Impulse());
            return;
        }
        transform.Translate(Vector3.back * Time.deltaTime*currentSpeedBox);
        PanelOnBox.transform.position = transform.position + Vector3.up;
    }

    private IEnumerator Impulse()
    { 
       var mat = gameObject.GetComponent<MeshRenderer>().material;
       mat.color = Color.red;
       isImpulse = true;
       yield return new WaitForSeconds(.5f);
       
        mat.color = Color.white;
        
        yield return new WaitForSeconds(.5f);
        isImpulse = false;
    }
}
