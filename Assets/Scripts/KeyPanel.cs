﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class KeyPanel : MonoBehaviour
{
    public event Action IsEmptyChars;
    public int StartCountKey { get; set; } = 3;
    public List<KeyChar> KeyChars { get; set; }

    [SerializeField] private KeyChar keyCharPrefab;


    private List<string> _characters;
    [Range(3, 10)] private int _countKey;
    
    
    private void Start()
    {
        _countKey = StartCountKey;
        KeyChars = new List<KeyChar>(_countKey);
        _characters = new List<string>(_countKey);
        for (int i = 0; i < _countKey; i++)
        {
            var newKey = Instantiate(keyCharPrefab, transform);
            KeyChars.Add(newKey);
            _characters.Add(newKey.Character);
        }
    }

    public void CheckAndDestroyKey(string keyCode)
    {
        foreach (var el in KeyChars)
        {
            if (el.Character == keyCode)
            {
                KeyChars.Remove(el);
                Destroy(el.gameObject);
                if (KeyChars.Count == 0)
                {
                    
                    Destroy(el.gameObject.transform.parent.gameObject);
                    IsEmptyChars?.Invoke();
                    break;
                }
            }
            else
            {
                Debug.Log("Wrong button is pressed!");
            }
        }
    }
}
